/**
 * 1. Напиши функцию createCounter, которая будет считать количество вызовов.
 * Нужно использовать замыкание.
 */

function createCounter() {
    let i = 0;
    return () => {
        return ++i;
    }
}

/**
 * 2. Не меняя уже написаный код (можно только дописывать новый),
 * сделай так, чтобы в calculateHoursOnMoon
 * переменная HOURS_IN_DAY была равна 29,5, а в функции calculateHoursOnEarth
 * эта переменная была 24.
 */

let HOURS_IN_DAY = 24;

function calculateHoursOnMoon(days) {
    const HOURS_IN_DAY = 29.5;
    return days * HOURS_IN_DAY;
}

function calculateHoursOnEarth(days) {
    return days * HOURS_IN_DAY;
}

/**
 * 3. Допиши функцию crashMeteorite, после которой
 * продолжительность дня на земле (из предыдущей задачи)
 * изменится на 22 часа
 */

function crashMeteorite() {
    HOURS_IN_DAY = 22;
}

/**
 * 4. Функция createMultiplier должна возвращать функцию, которая
 * при первом вызове возвращает произведение аргумента
 * функции createMultiplier и переменной a, при втором — аргумента и b,
 * при третьем — аргумента и c, с четвертого вызова снова a, потом b и так далее.
 *
 * Например:
 * const func = createMultiplier(2);
 * func(); // 16 (2*a)
 * func(); // 20 (2*b)
 * func(); // 512 (2*c)
 * func(); // 16 (2*a)
 *
 */
function createMultiplier(num) {
    const a = 8;
    const b = 10;
    const c = 256;
    let i = 0;
    return function foo() {
        let ans;
        switch (i) {
            case 0:
                ans = num * a;
                break;
            case 1:
                ans = num * b;
                break;
            case 2:
                ans = num * c;
                break;
            default:
                break;
        }
        i = (i + 1) % 3;
        return ans;
    }
}

/**
 * 5. Напиши функцию createStorage, которая будет уметь хранить
 * какие-то данные и манипулировать ими.
 * Функция должна возвращать объект с методами:
 * - add — метод, который принимает на вход любое количество аргументов и добавляет их в хранилище;
 * - get — метод, возвращающий хранилище;
 * - clear — метод, очищающий хранилище;
 * - remove — метод, который принимает на вход элемент, который нужно удалить из хранилища и удаляет его;
 *
 * Примеры использования смотри в тестах.
 */

function createStorage() {
    const set = [];
    return {
        add: function () {
            Array.prototype.forEach.call(arguments, (arg) => {
                set.push(arg);
            })
        },
        get: function () {
            return set;
        },
        clear: function () {
            set.splice(0);
        },
        remove: function (forRemove) {
            for (let i = set.length - 1; i > -1; i--) {
                if (set[i] === forRemove) {
                    set.splice(i, 1);
                }
            }
        }
    }
}

/**
 * 6*. Реализовать через let функцию поочередного
 * добавления в массив чисел от 0 до 10 с интервалом в 500ms.
 *
 * Для выполнения задачи должен быть цикл от 0 до 10,
 * внутри которого должен быть setTimeout(func, 50) с функцией внутри,
 * которая должна наполнить массив result числами от 0 до 10
*/

function letTimeout() {
    const result = [];


    for (let i = 0; i <= 10; i++) {
        setTimeout(() => {
            result.push(i);
        }, 500)
        // Напиши код, который через setTimeout дополнит result
    }

    return result; // числа от 0 до 10
}

/**
 * 7*. Реализовать такую же функцию, как letTimeout, только через var.
 * В комментарии объяснить, почему и как она работает
 */

function varTimeout() {
    const result = [];

    for (var i = 0; i <= 10; i++) {
        setTimeout(() => {
            result.push(i);/* заполнит массив числом 10 т.к. в отличие от варианта с let область видимости var одна
            и не пересоздается на каждой итерации,а for досчитает до 10 быстрее чем за 500 мс*/
        }, 500)
        // Напиши код, который через setTimeout дополнит result
    }

    return result; // числа от 0 до 10
}

module.exports = {
    calculateHours: {
        onEarth: calculateHoursOnEarth,
        onMoon: calculateHoursOnMoon,
    },
    crashMeteorite,
    createMultiplier,
    createStorage,
    createCounter,
    letTimeout,
    varTimeout
};
